<?php
        require_once("includes/config.php");
        require_once("includes/mysql.php");

        $json_request = json_decode(file_get_contents('php://input'), true);

        $query = new db_query();

        $input_info = array_pop($json_request);
        $input_info['scrambled']=$input_info['scrambled']?'true':'false';
        $input_info['onair']=$input_info['onair']?'true':'false';

        $ret = $query->result("update input set
                                      scrambled = '".$query->escape($input_info['scrambled'])."',
                                      cc_error = '".$query->escape($input_info['cc_error'])."',
                                      pes_error = '".$query->escape($input_info['pes_error'])."',
                                      bitrate = '".$query->escape($input_info['bitrate'])."',
                                      onair = '".$query->escape($input_info['onair'])."',
                                      last_update = now()
                                      where channel_id = '".$query->escape($input_info['channel_id'])."'
                                      and input_id = '".$query->escape($input_info['input_id'])."'");

?>

