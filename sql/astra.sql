-- MySQL dump 10.14  Distrib 5.5.32-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: astra
-- ------------------------------------------------------
-- Server version	5.5.32-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `astra_instance`
--

DROP TABLE IF EXISTS `astra_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `astra_instance` (
  `astra_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `control_server_addr` varchar(200) NOT NULL DEFAULT '',
  `control_server_iface` varchar(200) NOT NULL DEFAULT '',
  `control_server_port` int(11) unsigned NOT NULL DEFAULT '0',
  `event_request` varchar(200) NOT NULL DEFAULT '',
  `event_request_interval` int(11) unsigned NOT NULL DEFAULT '30',
  PRIMARY KEY (`astra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `channel_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `astra_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `enable` enum('true','false') NOT NULL DEFAULT 'true',
  `event` enum('true','false') NOT NULL DEFAULT 'true',
  PRIMARY KEY (`channel_id`),
  KEY `channel_astra_fk` (`astra_id`),
  CONSTRAINT `channel_astra_fk` FOREIGN KEY (`astra_id`) REFERENCES `astra_instance` (`astra_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `value_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `astra_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `value` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`value_id`),
  KEY `config_astra_fk` (`astra_id`),
  CONSTRAINT `config_astra_fk` FOREIGN KEY (`astra_id`) REFERENCES `astra_instance` (`astra_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dvb_input`
--

DROP TABLE IF EXISTS `dvb_input`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dvb_input` (
  `dvb_input_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('S','S2','T','T2','C','ASI') NOT NULL DEFAULT 'S',
  `name` varchar(50) NOT NULL DEFAULT '',
  `adapter` int(11) unsigned NOT NULL DEFAULT '0',
  `device` int(11) unsigned NOT NULL DEFAULT '0',
  `mac` varchar(17) NOT NULL DEFAULT '',
  `budget` enum('true','false') NOT NULL DEFAULT 'false',
  `buffer_size` int(11) unsigned NOT NULL DEFAULT '2',
  `modulation` enum('NONE','QPSK','QAM16','QAM32','QAM64','QAM128','QAM256','AUTO','VSB8','VSB16','PSK8','APSK16','APSK32','DQPSK','QPSK','QAM16','QAM32','QAM64','QAM128','QAM256','AUTO','VSB8','VSB16','PSK8','APSK16','APSK32','DQPSK') DEFAULT NULL,
  `fec` enum('NONE','1/2','2/3','3/4','4/5','5/6','6/7','7/8','8/9','AUTO','3/5','9/10') DEFAULT NULL,
  `frequency` int(11) unsigned NOT NULL DEFAULT '0',
  `polarization` enum('V','H','R','L') NOT NULL DEFAULT 'V' COMMENT 'S/S2',
  `symbolrate` int(11) unsigned NOT NULL DEFAULT '27500' COMMENT 'S/S2',
  `lof1` int(11) unsigned NOT NULL DEFAULT '10750' COMMENT 'S/S2',
  `lof2` int(11) unsigned NOT NULL DEFAULT '10750' COMMENT 'S/S2',
  `slof` int(11) unsigned NOT NULL DEFAULT '10750' COMMENT 'S/S2',
  `lnb_sharing` enum('true','false') NOT NULL DEFAULT 'false' COMMENT 'S/S2',
  `diseqc` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'S/S2',
  `tone` enum('true','false') NOT NULL DEFAULT 'false' COMMENT 'S/S2',
  `rolloff` enum('35','20','25','AUTO') DEFAULT NULL COMMENT 'S2',
  `bandwidth` enum('8mhz','7mhz','6mhz','AUTO') DEFAULT NULL COMMENT 'T/T2',
  `guardinterval` enum('1/32','1/16','1/8','1/4','AUTO') DEFAULT NULL COMMENT 'T/T2',
  `transmitmode` enum('2K','8K','AUTO','4K') DEFAULT NULL COMMENT 'T/T2',
  `hierarchy` enum('NONE','1','2','4','AUTO') DEFAULT NULL COMMENT 'T/T2',
  `no_sdt` enum('true','false') NOT NULL DEFAULT 'false',
  `no_eit` enum('true','false') NOT NULL DEFAULT 'false',
  `pass_sdt` enum('true','false') NOT NULL DEFAULT 'false',
  `pass_eit` enum('true','false') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`dvb_input_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `filter`
--

DROP TABLE IF EXISTS `filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filter` (
  `filter_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `input_id` int(11) unsigned NOT NULL DEFAULT '0',
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`filter_id`),
  KEY `filter_input_fk` (`input_id`),
  CONSTRAINT `filter_input_fk` FOREIGN KEY (`input_id`) REFERENCES `input` (`input_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `filter_template`
--

DROP TABLE IF EXISTS `filter_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filter_template` (
  `filter_template_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  UNIQUE KEY `filter_template_id` (`filter_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `filter_template_data`
--

DROP TABLE IF EXISTS `filter_template_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filter_template_data` (
  `filter_template_data_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filter_template_id` int(11) unsigned NOT NULL DEFAULT '0',
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`filter_template_data_id`),
  KEY `filter_templ_data_fk` (`filter_template_id`),
  CONSTRAINT `filter_tmpl_data_fk` FOREIGN KEY (`filter_template_id`) REFERENCES `filter_template` (`filter_template_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `input`
--

DROP TABLE IF EXISTS `input`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `input` (
  `input_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) unsigned NOT NULL DEFAULT '0',
  `priority` int(11) unsigned NOT NULL DEFAULT '0',
  `type` enum('dvb','udp','file','http') NOT NULL DEFAULT 'dvb',
  `loop` enum('true','false') NOT NULL DEFAULT 'false',
  `lock` varchar(500) NOT NULL DEFAULT '',
  `interface` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(50) NOT NULL DEFAULT '',
  `port` int(11) unsigned NOT NULL DEFAULT '1234',
  `rtp` enum('true','false') NOT NULL DEFAULT 'false',
  `socket_size` int(11) unsigned NOT NULL DEFAULT '0',
  `renew` int(11) unsigned NOT NULL DEFAULT '0',
  `dvb_input_id` int(11) unsigned NOT NULL DEFAULT '0',
  `path` varchar(512) NOT NULL DEFAULT '',
  `buffer_size` int(11) unsigned NOT NULL DEFAULT '0',
  `login` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `pnr` int(11) unsigned NOT NULL DEFAULT '0',
  `set_pnr` int(11) unsigned NOT NULL DEFAULT '0',
  `cas_pnr` int(11) unsigned NOT NULL DEFAULT '0',
  `biss` varchar(16) NOT NULL DEFAULT '',
  `cam` enum('newcamd','biss','hardware') DEFAULT NULL,
  `cas_data` varchar(20) NOT NULL DEFAULT '',
  `newcamd_id` int(11) unsigned NOT NULL DEFAULT '0',
  `onair` enum('true','false') NOT NULL DEFAULT 'false',
  `bitrate` int(11) unsigned NOT NULL DEFAULT '0',
  `scrambled` enum('true','false') NOT NULL DEFAULT 'false',
  `cc_error` int(11) unsigned NOT NULL DEFAULT '0',
  `pes_error` int(11) unsigned NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`input_id`),
  KEY `channel_newcamd_fk` (`channel_id`),
  CONSTRAINT `channel_input_fk` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`channel_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `map`
--

DROP TABLE IF EXISTS `map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map` (
  `map_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) unsigned NOT NULL DEFAULT '0',
  `input_pid` varchar(50) NOT NULL DEFAULT '',
  `output_pid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`map_id`),
  UNIQUE KEY `channel_output` (`channel_id`,`output_pid`),
  CONSTRAINT `cmap_channel_fk` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`channel_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `map_template`
--

DROP TABLE IF EXISTS `map_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_template` (
  `map_template_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`map_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `map_template_data`
--

DROP TABLE IF EXISTS `map_template_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_template_data` (
  `map_template_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `map_template_id` int(11) unsigned NOT NULL DEFAULT '0',
  `input_pid` varchar(50) NOT NULL DEFAULT '',
  `output_pid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`map_template_data_id`),
  KEY `map_templ_data_fk` (`map_template_id`),
  CONSTRAINT `map_templ_data_fk` FOREIGN KEY (`map_template_id`) REFERENCES `map_template` (`map_template_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mixaudio`
--

DROP TABLE IF EXISTS `mixaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mixaudio` (
  `mixaudio_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `output_id` int(11) unsigned NOT NULL DEFAULT '0',
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `mode` varchar(2) NOT NULL DEFAULT '',
  PRIMARY KEY (`mixaudio_id`),
  KEY `mixaudio_output_fk` (`output_id`),
  CONSTRAINT `mixaudio_output_fk` FOREIGN KEY (`output_id`) REFERENCES `output` (`output_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mpts`
--

DROP TABLE IF EXISTS `mpts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpts` (
  `mpts_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`mpts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newcamd`
--

DROP TABLE IF EXISTS `newcamd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newcamd` (
  `newcamd_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `host` varchar(50) NOT NULL DEFAULT '',
  `port` int(11) unsigned NOT NULL DEFAULT '0',
  `timeout` int(11) unsigned NOT NULL DEFAULT '10',
  `user` varchar(50) NOT NULL DEFAULT '',
  `pass` varchar(50) NOT NULL DEFAULT '',
  `key` varchar(50) NOT NULL DEFAULT '0102030405060708091011121314',
  `disable_emm` enum('true','false') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`newcamd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `output`
--

DROP TABLE IF EXISTS `output`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `output` (
  `output_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) unsigned NOT NULL DEFAULT '0',
  `type` enum('udp','file','http') NOT NULL DEFAULT 'udp',
  `interface` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(50) NOT NULL DEFAULT '',
  `port` int(11) unsigned NOT NULL DEFAULT '1234',
  `socket_size` int(11) unsigned NOT NULL DEFAULT '0',
  `buffer_size` int(11) unsigned NOT NULL DEFAULT '0',
  `buffer_fill` int(11) unsigned NOT NULL DEFAULT '0',
  `buffer_prefill` int(11) unsigned NOT NULL DEFAULT '0',
  `ttl` int(11) unsigned NOT NULL DEFAULT '0',
  `rtp` enum('true','false') NOT NULL DEFAULT 'false',
  `sync` int(11) unsigned NOT NULL DEFAULT '0',
  `path` varchar(512) NOT NULL DEFAULT '',
  `m2ts` enum('true','false') NOT NULL DEFAULT 'false',
  `biss` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`output_id`),
  KEY `output_channel_fk` (`channel_id`),
  CONSTRAINT `output_channel_fk` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`channel_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_password` (`user`,`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'astra'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-18 13:13:19
