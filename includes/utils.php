<?php
//Подготовка массива к выводу.
//-----------------------------------------
function htmlspecialchars_array(&$data)
{
     $data=array_map('htmlspecialchars_array_callback',$data);
}

function htmlspecialchars_array_callback($val)
{
	return htmlspecialchars(trim($val),ENT_QUOTES);
}


function ip_2_long($ip)
{
    return sprintf("%u",ip2long($ip));
}

function is_ip($ip) {
  $valid = TRUE;
  $ip = explode(".", $ip);
  if(count($ip)!=4) {
      return FALSE;
      }
  foreach($ip as $key => $block) {
      if(!is_numeric($block) || $block>=255 || ((!$key || $key==3) && $block<1) ) {
          $valid = FALSE;
      }
  }
  return $valid;
}


function is_ip_mask($ip) {
  $valid = TRUE;

  $ip = explode(".", $ip);
  if(count($ip)!=4) {
      return FALSE;
      }
  foreach($ip as $key => $block) {
      if(!is_numeric($block) || $block > 255 || ((!$key || $key == 3) && $block < 0) ) {
          $valid = FALSE;
      }
  }
  return $valid;
}

function array_to_json( $array ){

    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = '"'.addslashes($key).'"';

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = '"'.addslashes($value).'"';
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = '"'.addslashes($value).'"';
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }

    return $result;
} 

?>