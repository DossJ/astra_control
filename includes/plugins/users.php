<?php
  function plugin_users_main() {
          global $data;
          
          $query = new db_query();
          $query->result("select * from users order by user");
          
          $num = 0;
          
          $users = Array();
          $users['list'] = '';
          
          while (is_array($user = $query->fetch_assoc()))
          {
              $num++;  
              $user['num'] = $num;
              
              htmlspecialchars_array($user);
              
              $users['list'] .= template_parse('users/user.html',$user);
          }
          
          $data['page'] = template_parse('users/users_list.html',$users);
  }
  
  function plugin_users_delete_user() {
          global $data;
          
          $query = new db_query();
          $query->result("delete from users where user_id=".intval($data['user_id']));
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=users',true, 303);
          exit;
  }
  
  function plugin_users_add_user() {
          global $data;
          
          $query = new db_query();
          $query->result("insert into users set user = '".$query->escape($data['user'])."',
                                                password = '".$query->escape($data['password'])."'");
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=users',true, 303);
          exit;
  }
  
  function plugin_users_edit_user() {
          global $data;
          if(intval($data['user_id']))         
          {
              $query = new db_query();
              $query->result("update users set user = '".$query->escape($data['user'])."',
                                               password = '".$query->escape($data['password'])."'
                                               where user_id=".intval($data['user_id']));
          }
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=users',true, 303);
          exit;
  }
  
  
  
?>
