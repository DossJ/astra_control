<?php
  function plugin_astras_main() {
          global $data;
          
          $query = new db_query();
          $query->result("select * from astra_instance");
          
          $num = 0;
          
          $astras = Array();
          $astras['list'] = '';
          
          while (is_array($astra = $query->fetch_assoc()))
          {
              $num++;  
              $astra['num'] = $num;
              
              htmlspecialchars_array($astra);              
              $webdir = pathinfo($_SERVER['REQUEST_URI']);
              
              $astras['monitor_link'] = "http://".$_SERVER['HTTP_HOST'].$webdir['dirname']."/monitor.php";
              $astras['list'] .= template_parse('astras/astra.html',$astra);
          }
          
          $data['page'] = template_parse('astras/astras_list.html',$astras);
  }
  
  function plugin_astras_delete_astra() {
          global $data;
          
          $query = new db_query();
          $query->result("delete from astra_instance where astra_id=".intval($data['astra_id']));
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=astras',true, 303);
          exit;
  }
  
  function plugin_astras_add_astra() {
          global $data;
          
          $query = new db_query();
          $query->result("insert into astra_instance set name = '".$query->escape($data['name'])."',
                                                 control_server_addr = '".$query->escape($data['control_server_addr'])."',
                                                 control_server_iface = '".$query->escape($data['control_server_iface'])."',
                                                 control_server_port = '".$query->escape($data['control_server_port'])."',
                                                 event_request = '".$query->escape($data['event_request'])."',
                                                 event_request_interval = '".$query->escape($data['event_request_interval'])."'");
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=astras',true, 303);
          exit;
  }
  
    function plugin_astras_edit_astra() {
          global $data;
          if(intval($data['astra_id'])) {
              $query = new db_query();
              $query->result("update astra_instance set name = '".$query->escape($data['name'])."',
                                                     control_server_addr = '".$query->escape($data['control_server_addr'])."',
                                                     control_server_iface = '".$query->escape($data['control_server_iface'])."',
                                                     control_server_port = '".$query->escape($data['control_server_port'])."',
                                                     event_request = '".$query->escape($data['event_request'])."',
                                                     event_request_interval = '".$query->escape($data['event_request_interval'])."'
                                                     where astra_id=".intval($data['astra_id']));
          }
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=astras',true, 303);
          exit;
  }
  
  function plugin_astras_reload_astra() {
          global $data;
          if(intval($data['astra_id']))  
          {
                reload_astra($data['astra_id']);
          }
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=astras',true, 303);
          //exit;
  }
  

  
?>
