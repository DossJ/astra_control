<?php
  function plugin_camservers_main() {
          global $data, $lang;
          
          $query = new db_query();
          $query->result("select * from newcamd");
          
          $num = 0;
          
          $camservers = Array();
          $camservers['list'] = '';
          
          while (is_array($server = $query->fetch_assoc()))
          {
              $num++;  
              $server['num'] = $num;
 
              $server['disable_emm_text'] = $server['disable_emm']=='false'?$lang[LANG]['Yes']:$lang[LANG]['No'];
              htmlspecialchars_array($server);
              
              $camservers['list'] .= template_parse('camservers/server.html',$server);
          }
          
          $data['page'] = template_parse('camservers/servers_list.html',$camservers);
  }
  
  function plugin_camservers_delete_server() {
          global $data;
          
          $query = new db_query();
          $query->result("delete from newcamd where newcamd_id=".intval($data['newcamd_id']));
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=camservers',true, 303);
          exit;
  }
  
  function plugin_camservers_add_server() {
          global $data;
          
          $query = new db_query();
          $query->result("insert into newcamd set name = '".$query->escape($data['name'])."', 
                                                 host = '".$query->escape($data['host'])."',
                                                 port = '".$query->escape($data['port'])."',
                                                 user = '".$query->escape($data['user'])."',
                                                 pass = '".$query->escape($data['pass'])."',
                                                 timeout = '".$query->escape($data['timeout'])."',
                                                 `key` = '".$query->escape($data['key'])."',
                                                 disable_emm = '".$query->escape($data['disable_emm'])."'");
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=camservers',true, 303);
          exit;
  }
  
  function plugin_camservers_edit_server() {
          global $data;
          
          $query = new db_query();
          if (intval($data['newcamd_id']))
          {
              $query->result("update newcamd set name = '".$query->escape($data['name'])."', 
                                                     host = '".$query->escape($data['host'])."',
                                                     port = '".$query->escape($data['port'])."',
                                                     user = '".$query->escape($data['user'])."',
                                                     pass = '".$query->escape($data['pass'])."',
                                                     timeout = '".$query->escape($data['timeout'])."',
                                                     `key` = '".$query->escape($data['key'])."',
                                                     disable_emm = '".$query->escape($data['disable_emm'])."'
                                                     where newcamd_id=".intval($data['newcamd_id']));
          }
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=camservers',true, 303);
          exit;
  }
  
  
?>
