<?php


   if(isset($data['enable']) && $data['enable']) 
   {
     $data['enable']='true';
   }else{
     $data['enable']='false';
   }
          
   if(isset($data['event']) && $data['event']) 
   {
     $data['event']='true';
   }else{
     $data['event']='false';
   }
   
  function plugin_channels_main() {
          global $data, $lang;
          
          $query = new db_query();
          $query->result("select channel.`name` as channel_name, channel.channel_id as chid, astra_instance.name as astra_name, enable, event, astra_id, input.*, dvb_input.name,  output.output_id,
                                TIME_TO_SEC(timediff(now(),last_update)) as last_update_period
                                from channel
                                left join astra_instance using(astra_id)
                                left join output using(channel_id)
                                left join input on (channel.channel_id = input.channel_id)
                                left join dvb_input using(dvb_input_id)
                                order by channel.name,channel.channel_id, input.input_id, output.output_id, onair, priority,input_id
                                ");
          
          $num = 0;
          
          $channels = Array();
          $channels['list'] = '';
          
          while (is_array($channel = $query->fetch_assoc()))
          {
                  $num++;  
                  $channel['num'] = $num;
                  
                  if ($channel['bitrate'] < 100 ){
                      $channel['bitrate_bgcolor'] = '#FDD';
                  }
                  $channel['bitrate'] = intval($channel['bitrate'])." kBit";

                  
                  if ($channel['onair'] == 'true')
                  {
                      $channel['onair_title'] = $lang[LANG]['Connected'];
                      $channel['onair_image'] = 'connect.png';
                  }else{
                      $channel['onair_image'] = 'disconnect.png';
                      $channel['onair_bgcolor'] = 'yellow';
                      
                      if ($channel['scrambled'] == 'true' ) {
                          $channel['onair_title'] = $lang[LANG]['No channel on the satellite, or a wrong channel number'];
                      }else{
                          $channel['onair_title'] = $lang[LANG]['It is Not Working'];
                      }
                      
                  }
                  
                  if ($channel['scrambled'] == 'true' )
                  {
                      $channel['scrambled_title'] = $lang[LANG]['Encrypted'];
                      $channel['scrambled_image'] = 'lock.png';
                      $channel['scrambled_bgcolor'] = 'orange';
                  }else{
                      $channel['scrambled_title'] = $lang[LANG]['Unscrambled'];
                      $channel['scrambled_image'] = 'lock_unlock.png';
                  }
                  
                  if ($channel['last_update_period'] > 30*60 )
                  {
                      $channel['last_update_bgcolor'] = 'red';
                  }                  
                  
                  if ($channel['cc_error'] > 5 )
                  {
                      $channel['cc_error_bgcolor'] = '#C4DEFF';
                  }                  
                  
                  if ($channel['pes_error'] > 5 )
                  {
                      $channel['pes_error_bgcolor'] = '#C4DEFF';
                  }
                  $channel['channel_id'] = $channel['chid'];
                  htmlspecialchars_array($channel);
                  
                  if ($channel['enable'] == 'false') {
                     $channel['onair_bgcolor'] = '#CCCCCC';
                     $channel['scrambled_bgcolor'] = '#CCCCCC';
                     $channel['bitrate_bgcolor'] = '#CCCCCC';
                     $channel['cc_error_bgcolor'] = '#CCCCCC';
                     $channel['pes_error_bgcolor'] = '#CCCCCC';
                     $channel['last_update_bgcolor'] = '#CCCCCC';
                  }
                  
                  $channel['output'] = make_output_url_short($channel['output_id']);
                  $channel['input'] = make_input_url_short($channel['input_id']);
                                    
                  $channels['list'] .= template_parse('channels/channel.html',$channel);
          }
         
          $data['page'] = template_parse('channels/channels_list.html',$channels);
          
          $data['page'] .= "<script type='text/javascript'>
            
            var vTimeOut;
            function set_reload()
            {
                vTimeOut = setTimeout('location.reload()', ".(PAGE_RELOAD_TIME*1000).");
            }
            function stop_reload()
            {
                clearInterval(vTimeOut);
            }
            
            set_reload();
          </script>";
  }
  
  function plugin_channels_delete_channel() {
          global $data;

          $query = new db_query();
          if (intval($data['channel_id']))
          {
              $query->result("delete from channel where channel_id=".intval($data['channel_id']));
          } 
          header('Location: '.$_SERVER['PHP_SELF'].'?plugin=channels',true, 303);
          exit;
  }
  
  function plugin_channels_channel_info() {
          global $data;

          if (intval($data['channel_id']))
          {
                $query = new db_query();
                $channel = $query->assoc_array("select * from channel
                                                         where channel_id=".intval($data['channel_id']));
                htmlspecialchars_array($channel);
                
                $astra_select = new web_select();
                $astra_select->name='astra_id';
                $astra_select->title_column='name';
                $astra_select->value_column='astra_id';
                $astra_select->selected=$channel['astra_id'];
                
                $channel['enable_checked'] = $channel['enable'] == 'true'?'checked':'';
                $channel['event_checked'] = $channel['event'] == 'true'?'checked':'';
                
                
               $map_template_select = new web_select();
               $map_template_select->name='map_template_id';
               $map_template_select->title_column='name';
               $map_template_select->value_column='map_template_id';
                

                $query->result("select * from astra_instance order by name");
                $channel['astra_select'] = $astra_select->get_from_sql($query);
                $query->result("select * from map_template order by name");
                $channel['map_template_select'] = $map_template_select->get_from_sql($query);
          
                $data['page'] = template_parse('channels/channel_edit_form.html',$channel);
          
          }else{
              header('Location: '.$_SERVER['PHP_SELF'].'?plugin=channels',true, 303);
          }
  }
  
  
  function plugin_channels_add_channel() {
          global $data;
          
          if (intval($data['astra_id']))
          {
                $query = new db_query();
                $query->result("insert into channel set astra_id=".intval($data['astra_id']).",
                                       name='".$query->escape($data['name'])."',
                                       `enable`='".$query->escape($data['enable'])."',
                                       `event`='".$query->escape($data['event'])."'");

              $data['channel_id'] = $query->insert_id();
              header('Location: '.$_SERVER['PHP_SELF'].'?plugin=channels&action=channel_info&channel_id='.intval($data['channel_id']),true, 303);
          }else{
              header('Location: '.$_SERVER['PHP_SELF'].'?plugin=channels',true, 303);
          }

  }
  
 function plugin_channels_edit_channel() {
          global $data;
          if (intval($data['channel_id']) && intval($data['astra_id']))
          {
                $query = new db_query();
                $query->result("update channel set astra_id=".intval($data['astra_id']).",
                                       name='".$query->escape($data['name'])."',
                                       `enable`='".$query->escape($data['enable'])."',
                                       `event`='".$query->escape($data['event'])."'
                                       where channel_id=".intval($data['channel_id']));
          
              header('Location: '.$_SERVER['PHP_SELF'].'?plugin=channels&action=channel_info&channel_id='.intval($data['channel_id']),true, 303);
          }else{
              header('Location: '.$_SERVER['PHP_SELF'].'?plugin=channels',true, 303);
          }
  }  
  
  
 function plugin_channels_get_inputs() {
          global $data;
          $query = new db_query();
          
          if (intval($data['channel_id']))
          {
               $query->result("select * from input
                                    where channel_id=".intval($data['channel_id']).' order by priority');
                
               while(is_array($input = $query->fetch_assoc())) 
               {
                   $input['url'] = make_input_url($input['input_id']);
                   htmlspecialchars_array($input);
                   echo template_parse("channels/input_line.html", $input);
               }
                     
          }
          exit;
          
 }
 
 function plugin_channels_delete_input() {
          global $data;
          $query = new db_query();
          
          if (intval($data['input_id']))
          {
               $query->result("delete from input
                                    where input_id=".intval($data['input_id']));
          }
          exit;
          
 }
 
 
 function plugin_channels_get_maps() {
          global $data;
          $query = new db_query();
          
          if (intval($data['channel_id']))
          {
               $query->result("select * from map
                                        where channel_id=".intval($data['channel_id']));
                
               while(is_array($map = $query->fetch_assoc())) 
               {
                   htmlspecialchars_array($map);
                   echo template_parse("channels/map_line.html", $map);
               }
          }
          exit;
 }
 
 function plugin_channels_delete_map() {
          global $data;
          $query = new db_query();
          
          if (intval($data['map_id']))
          {
               $query->result("delete from map
                                      where map_id=".intval($data['map_id']));
          }
          exit;
 }
 
 function plugin_channels_delete_all_maps() {
          global $data;
          $query = new db_query();
          
          if (intval($data['channel_id']))
          {
               $query->result("delete from map
                                      where channel_id=".intval($data['channel_id']));
          }
          exit;
 }
 
 function plugin_channels_edit_map() {
          global $data;
          $query = new db_query();
          
          if (intval($data['map_id']))
          {
               $query->result("update map set input_pid='".$query->escape($data['input_pid'])."',
                                      output_pid='".intval($data['output_pid'])."'
                                      where map_id=".intval($data['map_id']));
          }
          exit;
 }
 
 function plugin_channels_add_map() {
          global $data;
          $query = new db_query();
          if (intval($data['channel_id']))
          {
                $query->result("insert into map set input_pid='".$query->escape($data['input_pid'])."',
                                       output_pid='".intval($data['output_pid'])."',
                                       channel_id=".intval($data['channel_id']));
          }
          exit;
 }
 
 function plugin_channels_add_map_template()
{
       global $data;
       $query = new db_query();
       if (intval($data['channel_id']) && intval($data['map_template_id']))
       { 
             $query->result("insert into map select 0,".intval($data['channel_id']).', input_pid, output_pid
                                    from map_template_data
                                    where map_template_id='.intval($data['map_template_id']));
               
       }
       exit;
}
 
 
 function plugin_channels_get_filters() {
          global $data;
          $query = new db_query();
          
          if (intval($data['channel_id']))
          {
               $query->result("select * from filter
                                        where channel_id=".intval($data['channel_id']));
                
               while(is_array($filter = $query->fetch_assoc())) 
               {
                   htmlspecialchars_array($filter);
                   echo template_parse("channels/filter_line.html", $filter);
               }
          }
          exit;
 }
 
  
  function plugin_channels_play_channel() {
          global $data;
          
          $query = new db_query();
          $channel = $query->assoc_array("select * from channel where channel_id=".intval($data['channel_id']));
          
          if(defined('UDPXY_HOST') || defined('UDPXY_HOSTONSERVER') )
          {
              //http://x.x.x.x:4050/udp/224.0.90.183:1234
              $host = defined('UDPXY_HOSTONSERVER')?$channel["server"]:UDPXY_HOST;
              $channel['output'] =  make_output_url_short(intval($data['output_id']));
          }else{
              $channel['output'] =  make_output_url_short(intval($data['output_id']));
          }

          echo template_parse("channels/player.html", $channel);
          exit;
  }
  
   function plugin_channels_get_outputs() {
          global $data;
          $query = new db_query();
          
          if (intval($data['channel_id']))
          {
               $query->result("select * from output
                                        where channel_id=".intval($data['channel_id']));
                
               while(is_array($output = $query->fetch_assoc())) 
               {
                   $output['url'] = make_output_url($output['output_id']);
                   htmlspecialchars_array($output);
                   echo template_parse("channels/output_line.html", $output);
               }
                     
          }
          exit;
          
 }
 
 function plugin_channels_delete_output() {
          global $data;
          $query = new db_query();
          
          if (intval($data['output_id']))
          {
               $query->result("delete from output
                                     where output_id=".intval($data['output_id']));
          }
          exit;
 }

 function plugin_channels_get_output_params() {
          global $data;
          
          $query = new db_query();
          //$ret='alert('.intval($data['output_id']).');';
          
          $output = $query->assoc_array("select * from output where output_id=".intval($data['output_id']));
          $ret='';
          if(is_array($output))
          {
              foreach($output as $key => $value )
              {
                  if($value=='true' || $value=='false')
                  {
                      if ($value == 'true') {
                          $ret.="\$('#output-edit-form #".$key."').prop('checked', true);\n";    
                      }else{
                          $ret.="\$('#output-edit-form #".$key."').prop('checked', false);\n";    
                      }
                  }else{
                      $ret.="\$('#output-edit-form #".$key."').val('".$value."');\n";    
                  }
                  //$ret.='alert("'.$key.'");';
              }
          }
          echo $ret;
          exit;
  }
  
  
  function plugin_channels_get_input_params() {
          global $data;
          
          $query = new db_query();
          //$ret='alert('.intval($data['input_id']).');';
          
          $input = $query->assoc_array("select * from input where input_id=".intval($data['input_id']));
          $ret='';
          if(is_array($input))
          {
              if ($input['cam']!='newcamd') $input['newcamd_id']='';
              if (!$input['set_pnr']) $input['set_pnr']='';
              if (!$input['cas_pnr']) $input['cas_pnr']='';
              
              foreach($input as $key => $value )
              {
                  if($value=='true' || $value=='false')
                  {
                      if ($value == 'true') {
                          $ret.="\$('#input-edit-form #".$key."').prop('checked', true);\n";    
                      }else{
                          $ret.="\$('#input-edit-form #".$key."').prop('checked', false);\n";    
                      }
                  }else{
                      $ret.="\$('#input-edit-form #".$key."').val('".$value."');\n";    
                  }
                  //$ret.='alert("'.$key.'");';
              }
          }
          echo $ret;
          exit;
  }
  
   function plugin_channels_edit_input() {
          global $data;
          
          $data['rtp']=isset($data['rtp'])?'true':'false';
          $data['loop']=isset($data['loop'])?'true':'false';
          
          if (intval($data['input_id']))
          {
                    
              $query = new db_query();
              if ($data['cam']!='newcamd') $data['newcamd_id']=0;
              //$ret='alert('.intval($data['output_id']).');';
              
              $output = $query->result("update input
                                                    set `type`='".$query->escape($data['type'])."', 
                                                    priority='".intval($data['priority'])."',
                                                    `loop`='".$query->escape($data['loop'])."',
                                                    `lock`='".$query->escape($data['lock'])."',
                                                    interface='".$query->escape($data['interface'])."', 
                                                    address='".$query->escape($data['address'])."', 
                                                    port='".intval($data['port'])."',
                                                    socket_size='".intval($data['socket_size'])."',
                                                    renew='".intval($data['renew'])."',
                                                    dvb_input_id='".intval($data['dvb_input_id'])."',
                                                    buffer_size = '".intval($data['buffer_size'])."',
                                                    login='".$query->escape($data['login'])."', 
                                                    password='".$query->escape($data['password'])."', 
                                                    pnr='".intval($data['pnr'])."',
                                                    set_pnr='".intval($data['set_pnr'])."',
                                                    cas_pnr='".intval($data['cas_pnr'])."',
                                                    rtp='".$query->escape($data['rtp'])."', 
                                                    path='".$query->escape($data['path'])."', 
                                                    biss='".$query->escape($data['biss'])."',
                                                    cam='".$query->escape($data['cam'])."',
                                                    cas_data='".$query->escape($data['cas_data'])."',
                                                    newcamd_id='".intval($data['newcamd_id'])."'
                                                    where input_id=".intval($data['input_id']));
          }
          exit;
  }
  
  function plugin_channels_add_input() {
          global $data;
          
          $data['rtp']=isset($data['rtp'])?'true':'false';
          $data['loop']=isset($data['loop'])?'true':'false';
          
          if (intval($data['channel_id']))
          {
                    
              $query = new db_query();
              //$ret='alert('.intval($data['output_id']).');';
              
              $output = $query->result("insert into input
                                                    set `type`='".$query->escape($data['type'])."', 
                                                    channel_id='".intval($data['channel_id'])."',
                                                    priority='".intval($data['priority'])."',
                                                    `loop`='".$query->escape($data['loop'])."',
                                                    `lock`='".$query->escape($data['lock'])."',
                                                    interface='".$query->escape($data['interface'])."', 
                                                    address='".$query->escape($data['address'])."', 
                                                    port='".intval($data['port'])."',
                                                    socket_size='".intval($data['socket_size'])."',
                                                    renew='".intval($data['renew'])."',
                                                    dvb_input_id='".intval($data['dvb_input_id'])."',
                                                    buffer_size = '".intval($data['buffer_size'])."',
                                                    login='".$query->escape($data['login'])."', 
                                                    password='".$query->escape($data['password'])."', 
                                                    pnr='".intval($data['pnr'])."',
                                                    set_pnr='".intval($data['set_pnr'])."',
                                                    cas_pnr='".intval($data['cas_pnr'])."',
                                                    rtp='".$query->escape($data['rtp'])."', 
                                                    path='".$query->escape($data['path'])."', 
                                                    biss='".$query->escape($data['biss'])."',
                                                    cam='".$query->escape($data['cam'])."',
                                                    cas_data='".$query->escape($data['cas_data'])."',
                                                    newcamd_id='".intval($data['newcamd_id'])."'");
          }
          exit;
  }
  
  
     function plugin_channels_get_dvb_input_select() {
          global $data;
          $query = new db_query();
          //$ret='alert('.intval($data['input_id']).');';
          
          $input = $query->result("select * from dvb_input order by name");
          $select = new web_select();
          //$select->options_only = true;
          $select->name="dvb_input_id";
          $select->title_column='name';
          $select->value_column='dvb_input_id';
          echo $select->get_from_sql($input);
          exit;
     }
     
     function plugin_channels_get_newcamd_select() {
          global $data;
          $query = new db_query();
          //$ret='alert('.intval($data['input_id']).');';
          
          $newcamd = $query->result("select * from newcamd order by name");
          $select = new web_select();
          //$select->options_only = true;
          $select->name = "newcamd_id";
          $select->title_column = 'name';
          $select->value_column = 'newcamd_id';
          $select->select_first = false;
          echo $select->get_from_sql($newcamd);
          exit;
     }
     
     function plugin_channels_get_astra_select() {
          global $data;
          $query = new db_query();
          //$ret='alert('.intval($data['input_id']).');';
          
          $astra = $query->result("select * from astra_instance order by name");
          $select = new web_select();
          //$select->options_only = true;
          $select->name = "astra_id";
          $select->title_column = 'name';
          $select->value_column = 'astra_id';
          $select->select_first = false;
          echo $select->get_from_sql($astra);
          exit;
     }
     
  
   function plugin_channels_add_output() {
          global $data;
         
          $data['rtp']=isset($data['rtp'])?'true':'false';
          $data['m2ts']=isset($data['m2ts'])?'true':'false';           
          
          $query = new db_query();
          //$ret='alert('.intval($data['output_id']).');';
          
          $output = @$query->result("insert into output
                                                set channel_id='".intval($data['channel_id'])."', 
                                                `type`='".$query->escape($data['type'])."', 
                                                interface='".$query->escape($data['interface'])."', 
                                                address='".$query->escape($data['address'])."', 
                                                port='".intval($data['port'])."',
                                                socket_size='".intval($data['socket_size'])."',
                                                buffer_size='".intval($data['buffer_size'])."',
                                                buffer_fill='".intval($data['buffer_fill'])."',
                                                buffer_prefill='".intval($data['buffer_prefill'])."',
                                                ttl='".intval($data['ttl'])."',
                                                rtp='".$query->escape($data['rtp'])."', 
                                                sync='".intval($data['sync'])."', 
                                                path='".$query->escape($data['path'])."', 
                                                m2ts='".$query->escape($data['m2ts'])."', 
                                                biss='".$query->escape($data['biss'])."'");
          exit;
  }
  
     function plugin_channels_edit_output() {
          global $data;
          
          $data['rtp']=isset($data['rtp'])?'true':'false';
          $data['m2ts']=isset($data['m2ts'])?'true':'false';           
          
          if (intval($data['output_id']))
          {
                    
              $query = new db_query();
              //$ret='alert('.intval($data['output_id']).');';
              
              $output = @$query->result("update output
                                                    set `type`='".$query->escape($data['type'])."', 
                                                    interface='".$query->escape($data['interface'])."', 
                                                    address='".$query->escape($data['address'])."', 
                                                    port='".intval($data['port'])."',
                                                    socket_size='".intval($data['socket_size'])."',
                                                    buffer_size='".intval($data['buffer_size'])."',
                                                    buffer_fill='".intval($data['buffer_fill'])."',
                                                    buffer_prefill='".intval($data['buffer_prefill'])."',
                                                    ttl='".intval($data['ttl'])."',
                                                    rtp='".$query->escape($data['rtp'])."', 
                                                    sync='".intval($data['sync'])."', 
                                                    path='".$query->escape($data['path'])."', 
                                                    m2ts='".$query->escape($data['m2ts'])."', 
                                                    biss='".$query->escape($data['biss'])."'
                                                    where output_id=".intval($data['output_id']));
          }
          exit;
  }

  
?>
